/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.videochat;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.websocket.server.ServerEndpoint;
import javax.websocket.OnOpen;
import javax.websocket.OnClose;
import javax.websocket.OnMessage;
import javax.websocket.OnError;
import javax.websocket.Session;

/**
 *
 * @author green
 */
@ServerEndpoint("/websocket")
public class Server {
    
    @OnOpen
    public void open(Session session) {
        //empty
    }
    
    @OnClose
    public void close(Session session) {
        //empty
    }

    @OnError
    public void onError(Throwable error) {
        Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, error);
    }
    
    @OnMessage
    public void onMessage(String message, Session session) throws IOException {
       // just broadcast every received message
       for (Session s : session.getOpenSessions()) {
           s.getBasicRemote().sendText(message);
       }
    }
}
