/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.app.model;

import java.util.Date;

/**
 *
 * @author green
 */
public class Personality {

    public enum Option {
        YES, NO, NOTMATTER
    }

    public enum Sex {
        MALE, FEMALE
    }

    public enum HairColor {
        BLONDE, RED, BROWN, BLACK
    }

    public enum EyeColor {
        BLUE, GREY, GREEN, BROWN, BLACK
    }

    public enum Ethnicity {
        CAUCASIAN, AISIAN, MEDITERRANIAN, MIDDLEEASTERN, AFRICAN, OTHER
    }

    public enum Religion {
        CHRISTIAN, ISLAM, HINDU, JUDAISM, NONE, OTHER
    }

    private Sex sex;
    private HairColor hairColor;
    private EyeColor eyeColor;
    private Ethnicity ethnicity;
    private Religion religion;
    private Date birthday;
    private int heigth; // in cm. used as minimum for preference
    private int weight; // in kg. used as maximum for preference
    private int iq;     //        used as minimum for preference
    private Option vigin;
    private Option alcohol;
    private Option smoking;
    
    /**
     * @return the birthday
     */
    public Date getBirthday() {
        return birthday;
    }

    /**
     * @param birthday the birthday to set
     */
    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    /**
     * @return the sex
     */
    public Sex getSex() {
        return sex;
    }

    /**
     * @param sex the sex to set
     */
    public void setSex(Sex sex) {
        this.sex = sex;
    }

    /**
     * @return the hairColor
     */
    public HairColor getHairColor() {
        return hairColor;
    }

    /**
     * @param hairColor the hairColor to set
     */
    public void setHairColor(HairColor hairColor) {
        this.hairColor = hairColor;
    }

    /**
     * @return the eyeColor
     */
    public EyeColor getEyeColor() {
        return eyeColor;
    }

    /**
     * @param eyeColor the eyeColor to set
     */
    public void setEyeColor(EyeColor eyeColor) {
        this.eyeColor = eyeColor;
    }

    /**
     * @return the ethnicity
     */
    public Ethnicity getEthnicity() {
        return ethnicity;
    }

    /**
     * @param ethnicity the ethnicity to set
     */
    public void setEthnicity(Ethnicity ethnicity) {
        this.ethnicity = ethnicity;
    }

    /**
     * @return the religion
     */
    public Religion getReligion() {
        return religion;
    }

    /**
     * @param religion the religion to set
     */
    public void setReligion(Religion religion) {
        this.religion = religion;
    }

    /**
     * @return the heigth in cm
     */
    public int getHeigth() {
        return heigth;
    }

    /**
     * @param heigth the heigth to set in cm
     */
    public void setHeigth(int heigth) {
        this.heigth = heigth;
    }

    /**
     * @return the weight in kg
     */
    public int getWeight() {
        return weight;
    }

    /**
     * @param weight the weight to set in kg
     */
    public void setWeight(int weight) {
        this.weight = weight;
    }

    /**
     * @return the iq
     */
    public int getIq() {
        return iq;
    }

    /**
     * @param iq the iq to set
     */
    public void setIq(int iq) {
        this.iq = iq;
    }

    /**
     * @return the vigin
     */
    public Option getVigin() {
        return vigin;
    }

    /**
     * @param vigin the vigin to set
     */
    public void setVigin(Option vigin) {
        this.vigin = vigin;
    }

    /**
     * @return the alcohol
     */
    public Option getAlcohol() {
        return alcohol;
    }

    /**
     * @param alcohol the alcohol to set
     */
    public void setAlcohol(Option alcohol) {
        this.alcohol = alcohol;
    }

    /**
     * @return the smoking
     */
    public Option getSmoking() {
        return smoking;
    }

    /**
     * @param smoking the smoking to set
     */
    public void setSmoking(Option smoking) {
        this.smoking = smoking;
    }
}
