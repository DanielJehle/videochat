package org.app.model;

// TODO: Scheme own, preference redo own info and own sheme class

/**
 * User model
 * @author green
 */
public class User {
    private long id;
    private String number;
    private String name;
    private String password; // TODO Password
    private Personality personality;
    private Personality preference;
    private int plz;
    
    public User(long id, String number, String name, int plz) {
        this.id = id;
        this.number = number;
        this.name = name;
        this.plz = plz;
    }
    
    public void save() {}
    
    /**
    * @return the plz
    */
    public int getPlz() {
        return plz;
    }

    /**
     * @param plz the plz to set
     */
    public void setPlz(int plz) {
        this.plz = plz;
    }
    
    /**
     * @return the id
     */
    public long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * @return the mobile
     */
    public String getNumber() {
        return number;
    }

    /**
     * @param number mobile
     */
    public void setNumber(String number) {
        this.number = number;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the character
     */
    public Personality getPersonality() {
        return personality;
    }

    /**
     * @param character the character to set
     */
    public void setPersonality(Personality personality) {
        this.personality = personality;
    }

    /**
     * @return the preference
     */
    public Personality getPreference() {
        return preference;
    }

    /**
     * @param preference the preference to set
     */
    public void setPreference(Personality preference) {
        this.preference = preference;
    }
}
