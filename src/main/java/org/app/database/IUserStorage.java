package org.app.database;

import java.util.Set;
import org.app.errors.InvalidUserException;
import org.app.model.User;

/**
 *
 * @author green
 */
public interface IUserStorage {
    public long generateId();
    public User getById(long id);
    public User getByNumber(String number);
    public void store(User user) throws InvalidUserException;
    public void deleteById(long id);
    public Set<User> getUsers();
}
