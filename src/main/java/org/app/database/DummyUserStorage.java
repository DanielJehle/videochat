/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.app.database;

import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.atomic.AtomicLong;
import org.app.errors.InvalidUserException;
import org.app.model.User;

/**
 *
 * @author green
 */
public class DummyUserStorage implements IUserStorage {
    private  Set<User> DB = new HashSet<User>();
    private AtomicLong idCount = new AtomicLong(1);
    
    @Override
    public long generateId() {
        return idCount.getAndIncrement();
    }

    @Override
    public User getById(long id) {
        for (User u : DB) {
            if (u.getId() == id)
                return u;
        }
        return null;
    }

    @Override
    public User getByNumber(String number) {
        for (User u : DB) {
            if (u.getNumber() == number)
                return u;
        }
        return null;
    }
    
    public User getByName(String name) {
        for (User u : DB) {
            if (u.getName().equals(name))
                return u;
        }
        return null;
    }

    @Override
    public void store(User user) throws InvalidUserException {
        User byId = getById(user.getId());
        User byNumber = getByNumber(user.getNumber());
        User byName = getByName(user.getName());
        
        if (byId != null)
            throw new InvalidUserException("user.id " + user.getId() + " already exists.");
        else if (byNumber != null)
            throw new InvalidUserException("user.number " + user.getNumber() + " already exists.");
        else if (byName != null)
            throw new InvalidUserException("user.name " + user.getName() + " already exists.");
        else
            DB.add(user);
    }

    @Override
    public void deleteById(long id) {
        
    }

    @Override
    public Set<User> getUsers() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
