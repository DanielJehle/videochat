/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test.database;

import java.util.HashSet;
import java.util.Set;
import org.app.database.DummyUserStorage;
import org.app.errors.InvalidUserException;
import org.app.model.Personality;
import org.app.model.User;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Ignore;
import org.app.database.IUserStorage;

// TODO: use Mockito

/**
 *
 * @author green
 */
public class UserStorageTest {
    private static IUserStorage users;
    
    @Before
    public void setUp() {
        users = new DummyUserStorage();
    }
    
    @Test
    public void generateId() throws InvalidUserException {
        Set<Long> set = new HashSet<>();
        
        for (int i = 0; i < 1000; i++) {
            long id = users.generateId();
            users.store(new User(id, String.valueOf(i), "name" + i, 0));
            assertTrue(set.add(id));
        }
    }
   
    @Test
    public void storeNewUser() {
        try {
            User user = new User(1, "0123456789", "eric", 78315);
            users.store(user);
            User result = users.getById(user.getId());
            
            if (result == null) {
                fail("user1 not found in UserStorage.");
            }
            
            assertEquals(result.getId(), user.getId());
            assertEquals(result.getName(), user.getName());
            assertEquals(result.getNumber(), user.getNumber());
            assertEquals(result.getPlz(), user.getPlz());
            
        } catch(InvalidUserException ex) {
            fail(ex.getMessage());
        }
    }
    
    @Test
    public void storeUserExistingId() {
        boolean failed = true;
        try {
            User user = new User(1, "0123456789", "eric", 78315);
            User user2 = new User(1, "987654321", "john", 78315);
            users.store(user); // store success
            users.store(user2); // store error
        } catch(InvalidUserException ex) {
            failed = false;
        }
        if (failed) {
            fail("user stored");
        }
    }
    
    @Test
    public void storeUserExistingNumber() {
        boolean failed = true;
        try {
            User user = new User(1, "0123456789", "eric", 78315);
            User user2 = new User(2, "0123456789", "john", 78315);
            users.store(user); // store success
            users.store(user2); // store error
        } catch(InvalidUserException ex) {
            failed = false;
        }
        if (failed) {
            fail("user stored");
        }
    }
    
    @Test
    public void storeUserExistingName() {
        boolean failed = true;
        try {
            User user = new User(1, "0123456789", "eric", 78315);
            User user2 = new User(2, "987654321", "eric", 78315);
            users.store(user); // store success
            users.store(user2); // store error
        } catch(InvalidUserException ex) {
            failed = false;
        }
        if (failed) {
            fail("user stored");
        }
    }

    
    @Test
    public void getById() {
        User user = new User(1, "0123456789", "eric", 78315);
        try {
            users.store(user);            
        } catch(InvalidUserException ex) {
            fail("could not store user.");
        }
        
        User result = users.getById(user.getId());
        assertNotNull(result);
        
        result = users.getById(-200);
        assertNull(result);
    }
}